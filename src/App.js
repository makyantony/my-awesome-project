import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
        <div className="container">
            <div className="camada">
                <div>
                    <div className="item">
                        <div className="text">
                            Valorização do aluguel 12 meses
                        </div>
                        <div className="numeros">
                            10,29%
                        </div>
                    </div>
                </div>
                <div>
                    <div className="item">
                        <div className="text">
                            Lucro bruto do imóvel(Anual)
                        </div>
                        <div className="numeros">
                            0,41
                        </div>
                    </div>
                </div>
                <div>
                    <div className="item">
                        <div className="text">
                            Amostra de venda
                        </div>
                        <div className="numeros">
                            3.123
                        </div>
                    </div>
                </div>
            </div>
            <div className="camada">
                <div>
                    <div className="item">
                        <div className="text">
                            Rentabilidade do aluguel(Mês)
                        </div>
                        <div className="numeros">
                            0,4
                        </div>
                    </div>
                </div>
                <div>
                    <div className="item">
                        <div className="text">
                            Rentabilidade anual do imóvel
                        </div>
                        <div className="numeros">
                            0,4
                        </div>
                    </div>
                </div>
                <div>
                    <div className="item">
                        <div className="text">
                            Amostra de aluguel
                        </div>
                        <div className="numeros">
                            2.345
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

export default App;
